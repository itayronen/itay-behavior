# itay-behavior
Behavior tree, written in typescript.

# Goals
* Share trees between multiple agents.
* Performance:
	* Avoid ticking the tree when not needed.
	* Avoid ticking unnecessary nodes.
* Modular - Share subtrees by reference.
* Integrade State-Machine concepts to handle states, instead of representing states as prioritised branches.
* Support blackboard.

# Usage
```ts

```