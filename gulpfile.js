"use strict";
let gulp = require("gulp");
let ts = require('gulp-typescript');
let tsPackage = require("gulp-ts-package").default;
let del = require("del");
let changed = require("itay-gulp-changed");
let merge = require("merge2");
var gulpif = require('gulp-if');
let manglePrivate = require("ts-mangle-private").default;
let uglify = require("gulp-uglify-es").default;

let config = {
    tsconfig: "./src/tsconfig.json",
    tsGlob: "./src/**/*.ts",
    packageEntriesGlob: ["./src/index.ts"],
    dest: "./lib",
    bundles: "./bundles",
    packageName: require("./package.json").name,
    get compilerOptions() { return require(this.tsconfig).compilerOptions; }
};

gulp.task("build", function () {
    let tsProject = ts.createProject(config.tsconfig, { declaration: true });

    let tsResult = gulp.src(config.tsGlob)
        .pipe(changed())
        .pipe(tsProject());

    tsResult.on("error", () => {
        changed.reset();
        throw "Typescript build failed.";
    });

    return merge([
        tsResult.js.pipe(gulp.dest(config.dest)),
        tsResult.dts.pipe(gulp.dest(config.dest)),
    ]);
});

gulp.task("package", () => {
    let compilerOptions = config.compilerOptions;

    let streams = [];

    ["amd"].forEach(moduleFormat => {
        ["standard", "min"].forEach(type => {
            compilerOptions.module = moduleFormat;
            compilerOptions.outFile = type == "min" ?
                `bundle.${moduleFormat}.min.js` :
                `bundle.${moduleFormat}.js`;

            let tsProject = ts.createProject(compilerOptions);

            streams.push(gulp.src(config.packageEntriesGlob)
                .pipe(gulpif(type == "min", manglePrivate()))
                .pipe(tsProject())
                .pipe(tsPackage({ name: config.packageName, mainModule: "index" }))
                .pipe(gulpif(type == "min", uglify()))
                .pipe(gulp.dest(config.bundles))
            );
        });
    });

    return merge(streams);
});

gulp.task("clean", () => del([config.dest, config.bundles, "./.localStorage"]));