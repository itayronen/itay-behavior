import { Task, CancelToken, Status } from "task-run";
import { Node } from "./Node";
import { TickParams } from "./TickParams";
import { Event } from "itay-events";

class AgentState {
	tickParams: TickParams;
	runningTask: Task<void> | undefined;
}

class MyTickParams implements TickParams {
	public onUpdate = new Event();
	public cancelToken = CancelToken.none;

	constructor(public tree: BehaviorTree, public agent: any) {
	}

	public recycle(): void {
		this.onUpdate = new Event();
	}
}

export class BehaviorTree {
	public label: string = BehaviorTree.name;
	public child: Node;

	private agentStateSymbol = Symbol("Last tick result");

	constructor() {
	}

	public tick(agent: any): Task<void> {
		let agentState: AgentState | undefined = agent[this.agentStateSymbol];

		if (!agentState) {
			agentState = new AgentState();
			agent[this.agentStateSymbol] = agentState;

			agentState.tickParams = new MyTickParams(this, agent);
		}

		if (agentState.runningTask && agentState.runningTask.isRunning) {
			agentState.tickParams.onUpdate.trigger();

			let result = agentState.runningTask;

			if (agentState.runningTask.isEnded) {
				agentState.runningTask = undefined;
			}

			return result;
		}
		else {
			return this.fullTick(agentState);
		}
	}

	private fullTick(agentState: AgentState): Task<void> {
		(agentState.tickParams as MyTickParams).recycle();

		let result = this.child.tick(agentState.tickParams);

		// Handle failed ticks.
		// Happens when a tick is cancelled, or,
		// when the tree had no "fallback node" (kind of a bug in the tree design).
		result.end.catch(reason => { });

		if (result.isRunning) {
			agentState.runningTask = result;
		}

		return result;
	}
}