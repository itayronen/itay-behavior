import { CancelToken } from "task-run";
import { BehaviorTree } from "./BehaviorTree";
import { Event } from "itay-events";

export interface TickParams {
	readonly agent: any;
	readonly tree: BehaviorTree;
	readonly onUpdate: Event;
	readonly cancelToken: CancelToken;
}