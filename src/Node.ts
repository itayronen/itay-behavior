import { TickParams } from "./TickParams";
import { Task } from "task-run";

export interface Node {
	tick(params: TickParams): Task<void>;
	readonly label: string;
}