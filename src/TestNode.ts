import { Status, Task, Tasks } from "task-run";
import { Node } from "./Node";
import { TickParams } from "./TickParams";

export class TestNode implements Node {
	public label: string;
	public tickCount = 0;

	private isRunning = false;
	private updatesCount = 0;

	public isCancelled = false;
	public finalStatus: Status;

	constructor(finalStatus?: Status) {
		this.finalStatus = finalStatus || Status.Succeeded;

		if (this.finalStatus == Status.Running) {
			throw new Error(`The final state cannot be ${Status[Status.Running]}. Use ${this.setRunning.name}().`);
		}

		this.label = "TestNode. Final status: " + Status[this.finalStatus];
	}

	public setRunning(updatesCount: number): void {
		if (updatesCount < 0)
			throw new Error("updatesCount should be 0 or greater.");

		this.isRunning = true;
		this.updatesCount = updatesCount;
	}

	public tick(params: TickParams): Task<void> {
		this.tickCount++;

		if (this.isRunning) {
			let updatesCount = this.updatesCount;

			if (updatesCount == 0) {
				return Tasks.run(async () => {
					if (this.finalStatus == Status.Failed) throw "failed";
				});
			}

			let controller = Tasks.createController<void>();

			params.cancelToken.onCancel.add(() => {
				this.isCancelled = true;
				controller.setCancelled();
			});

			params.onUpdate.add(() => {
				updatesCount--;

				if (updatesCount == 0) {
					if (this.finalStatus == Status.Succeeded) {
						controller.setSucceded(undefined);
					}
					else {
						controller.setFailed();
					}
				}
			});

			return controller.task;
		}
		else if (this.finalStatus == Status.Succeeded) {
			return Tasks.succeded<void>(undefined);
		}
		else {
			return Tasks.failed("TestNode fail.");
		}
	}
}