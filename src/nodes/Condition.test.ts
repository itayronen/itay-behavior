import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { Event } from "itay-events";
import { Status, CancelToken } from "task-run";
import { TestNode, TickParams } from "./imports";
import { Condition } from "./Condition";

function createTickParams(): TickParams {
	return { agent: {}, tree: null!, cancelToken: CancelToken.none, onUpdate: new Event() };
}

export default function (suite: TestSuite): void {
	suite.describe(Condition.name, suite => {
		suite.describe("tick", suite => {
			suite.describe("When predicate true", suite => {

				suite.test("When no child, Success.", test => {
					test.arrange();
					let condition = new Condition(() => true);

					test.assert();
					assert.equal(condition.tick(null!).status, Status.Succeeded);
				});

				suite.test("When has child, returns child status.", test => {
					test.arrange();
					let testNode = new TestNode();
					testNode.setRunning(1);
					let condition = new Condition(() => true, testNode);

					test.assert();
					assert.equal(condition.tick(createTickParams()).status, Status.Running);
				});

				suite.test("When child is running, Update ok.", test => {
					test.arrange();
					let testNode = new TestNode();
					testNode.setRunning(1);
					let condition = new Condition(() => true, testNode);
					let params = createTickParams();

					test.act();
					let result = condition.tick(params);

					test.assert();
					assert.equal(result.status, Status.Running);
					params.onUpdate.trigger();
					assert.equal(result.status, Status.Succeeded);
				});
			});

			suite.describe("When predicate false", suite => {

				suite.test("When no else-child, Failed.", test => {
					test.arrange();
					let condition = new Condition(() => false);
					let params = createTickParams();

					test.act();
					let tickResult = condition.tick(params);
					tickResult.catch(() => { });

					test.assert();
					assert.equal(tickResult.status, Status.Failed);
				});

				suite.test("When has else-child, return child status.", test => {
					test.arrange();
					let testNode = new TestNode();
					testNode.setRunning(1);
					let condition = new Condition(() => false, undefined, testNode);
					let params = createTickParams();

					test.act();
					let tickResult = condition.tick(params);
					tickResult.catch(() => { });

					test.assert();
					assert.equal(tickResult.status, Status.Running);
				});
			});
		});
	});
}