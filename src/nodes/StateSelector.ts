import { Task, Tasks, Status, CancelTokenSource } from "task-run";
import { Event } from "itay-events";
import { Node, TickParams } from "./imports";

export class StateSelector implements Node {
	public label: string = StateSelector.name;
	public stateToChildMap: Map<any, Node>;

	private childUpdateListenerSymbol = Symbol();
	private childCancelSourceSymbol = Symbol();

	constructor(private getState: (params: TickParams) => any) {
		this.stateToChildMap = new Map();
	}

	public tick(params: TickParams): Task<void> {
		return Tasks.run<void>(async () => {
			let state: any = this.getState(params);

			let checkStateChange = () => {
				let newState = this.getState(params);
				if (newState != state) {
					state = newState;

					if (this.isInState(params))
						this.cancelState(params);
				}
			};

			params.onUpdate.add(checkStateChange);

			params.cancelToken.onCancel.add(() => {
				if (this.isInState(params))
					this.cancelState(params);

				params.onUpdate.remove(checkStateChange);
			});

			while (true) {
				let currentState = state;
				let stateTask = this.enterState(currentState, params);
				await stateTask.end;

				params.cancelToken.throwIfCancelRequested();

				if (currentState != state) {
					continue;
				}

				if (stateTask.isFailed)
					throw new Error(`StateSeletor failed. ${JSON.stringify(stateTask.error.message || stateTask.error)}`);

				break;
			}
		});
	}

	private enterState(state: any, params: TickParams): Task<void> {
		let childNode = this.stateToChildMap.get(state);

		if (!childNode) {
			throw new Error(`No child found for state: ${JSON.stringify(state)}`);
		}

		let childCancelSource = new CancelTokenSource();
		params.agent[this.childCancelSourceSymbol] = childCancelSource;

		let childUpdateEvent = new Event();

		let childParams: TickParams = {
			agent: params.agent,
			tree: params.tree,
			cancelToken: childCancelSource.token,
			onUpdate: childUpdateEvent
		};

		let updateChildFunc = () => {
			childUpdateEvent.trigger();
		};
		params.agent[this.childUpdateListenerSymbol] = updateChildFunc;
		params.onUpdate.add(updateChildFunc);

		return childNode.tick(childParams);
	}

	private cancelState(params: TickParams): void {
		let cancelSource: CancelTokenSource = params.agent[this.childCancelSourceSymbol];
		cancelSource.cancel();
		params.agent[this.childCancelSourceSymbol] = null;

		params.onUpdate.remove(params.agent[this.childUpdateListenerSymbol]);
	}

	private isInState(params: TickParams): boolean {
		return !!params.agent[this.childCancelSourceSymbol];
	}
}