import { Event } from "itay-events";
import { Task, Tasks, Status, TaskController } from "task-run";
import { Node, TickParams, ChildTickParams } from "./imports";

export class Sequence implements Node {
	public label: string = Sequence.name;
	public children: Node[];

	constructor(children?: Node[]) {
		this.children = children || [];
	}

	public tick(params: TickParams): Task<void> {
		if (params.cancelToken.isCancelRequested)
			return Tasks.cancelled();

		let controller = new TaskController<void>();

		this.sequence(controller, params);

		return controller.task;
	}

	private async sequence(controller: TaskController<void>, params: TickParams): Promise<void> {
		for (let i = 0; i < this.children.length; i++) {
			let childParams = new ChildTickParams(params);
			let task = this.children[i].tick(childParams);

			if (task.isRunning) {
				await task.end;
			}

			childParams.dispose();

			if (task.isCancelled || params.cancelToken.isCancelRequested) {
				controller.setCancelled();
				return;
			}

			if (task.isFailed) {
				controller.setFailed(task.error);
				return;
			}
		}

		controller.setSucceded(undefined);
	}
}