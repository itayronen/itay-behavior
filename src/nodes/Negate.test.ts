import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { Event } from "itay-events";
import { Status, CancelToken } from "task-run";
import { TestNode, TickParams } from "./imports";
import { Negate } from "./Negate";

function createTickParams(): TickParams {
	return { agent: {}, tree: null!, cancelToken: CancelToken.none, onUpdate: new Event() };
}

export default function (suite: TestSuite): void {
	suite.describe(Negate.name, suite => {
		suite.describe("tick", suite => {
			suite.test("When child succeeds, fail.", test => {
				test.arrange();
				let testNode = new TestNode(Status.Succeeded);
				let negate = new Negate(testNode);
				let params = createTickParams();

				test.act();
				let tickResult = negate.tick(params);
				tickResult.catch(() => { });

				test.assert();
				assert.equal(tickResult.status, Status.Failed);
			});

			suite.test("When child fails, succeed.", test => {
				test.arrange();
				let testNode = new TestNode(Status.Failed);
				let negate = new Negate(testNode);
				let params = createTickParams();

				test.act();
				let tickResult = negate.tick(params);
				tickResult.catch(() => { });

				test.assert();
				assert.equal(tickResult.status, Status.Succeeded);
			});

			suite.test("When child is running then fails, succeed.", async test => {
				test.arrange();
				let testNode = new TestNode(Status.Failed);
				testNode.setRunning(1);
				let negate = new Negate(testNode);
				let params = createTickParams();

				test.act();
				let tickResult = negate.tick(params);
				tickResult.catch(() => { });
				assert.equal(tickResult.status, Status.Running);
				params.onUpdate.trigger();

				await tickResult;

				test.assert();
				assert.equal(tickResult.status, Status.Succeeded);
			});
		});
	});
}