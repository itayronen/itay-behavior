import { Task, Tasks, Status, TaskController } from "task-run";
import { Node, TickParams, ChildTickParams } from "./imports";

export class Selector implements Node {
	public label: string = Selector.name;
	public children: Node[];

	constructor(children?: Node[]) {
		this.children = children || [];
	}

	public tick(params: TickParams): Task<void> {
		if (params.cancelToken.isCancelRequested)
			return Tasks.cancelled();

		let controller = new TaskController<void>();

		this.selector(controller, params);

		return controller.task;
	}

	private async selector(controller: TaskController<void>, params: TickParams): Promise<void> {
		for (let i = 0; i < this.children.length; i++) {
			let childParams = new ChildTickParams(params);
			let task = this.children[i].tick(childParams);

			if (task.isRunning) {
				await task.end;
			}

			childParams.dispose();

			if (params.cancelToken.isCancelRequested) {
				task.catch(() => { });
				controller.setCancelled();
				return;
			}

			if (task.isSucceded) {
				controller.setSucceded(undefined);
				return;
			}
		}

		controller.setFailed("No child succeded.");
	}
}