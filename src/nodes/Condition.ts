import { Task, Tasks, Status } from "task-run";
import { Node, TickParams } from "./imports";

export class Condition implements Node {
	public label: string = Condition.name;

	constructor(public predicate: (params: TickParams) => boolean, public child?: Node, public elseChild?: Node) {
	}

	public tick(params: TickParams): Task<void> {
		let predicateResult = this.predicate(params);

		if (!predicateResult) {
			if (this.elseChild) {
				return this.elseChild.tick(params);
			}
			else {
				return Tasks.failed<void>("Predicate is false.");
			}
		}
		else {
			if (this.child) {
				return this.child.tick(params);
			}
			else {
				return Tasks.succeded<void>(undefined);
			}
		}
	}
}