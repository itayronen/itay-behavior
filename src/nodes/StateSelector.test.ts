import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { Event } from "itay-events";
import { Status, CancelToken, CancelTokenSource } from "task-run";
import { TestNode, TickParams } from "./imports";
import { StateSelector } from "./StateSelector";

function createTickParams(cancelSource?: CancelTokenSource): TickParams {
	return {
		agent: {},
		tree: null!,
		cancelToken: cancelSource ? cancelSource.token : CancelToken.none,
		onUpdate: new Event()
	};
}

export default function (suite: TestSuite): void {
	suite.describe(StateSelector.name, suite => {
		suite.describe("tick", suite => {

			suite.test("When state is not mapped, fail.", async test => {
				test.arrange();
				let stateSelector = new StateSelector(() => "a");
				let params = createTickParams();

				test.act();
				let result = stateSelector.tick(params);
				result.catch(() => { });

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(result.status, Status.Failed);
			});

			suite.test("When a state runs and succeeds, succeed.", async test => {
				test.arrange();
				let stateSelector = new StateSelector(() => "a");
				let nodeA = new TestNode();
				nodeA.setRunning(1);
				stateSelector.stateToChildMap.set("a", nodeA);

				let params = createTickParams();

				test.act();
				let result = stateSelector.tick(params);

				await new Promise(resolve => setTimeout(resolve));

				assert.isTrue(result.isRunning);
				params.onUpdate.trigger();

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(result.status, Status.Succeeded);
			});

			suite.test("When cancelled, trigger child cancel immediately.", async test => {
				test.arrange();
				let stateSelector = new StateSelector(() => "a");
				let nodeA = new TestNode();
				nodeA.setRunning(10);
				stateSelector.stateToChildMap.set("a", nodeA);

				let cancelSource = new CancelTokenSource();
				let params = createTickParams(cancelSource);

				test.act();
				let result = stateSelector.tick(params);
				result.catch(() => { });
				assert.isTrue(result.isRunning);

				cancelSource.cancel();

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(result.status, Status.Cancelled);
			});

			suite.test("When a state runs and fails, fail.", async test => {
				test.arrange();
				let stateSelector = new StateSelector(() => "a");
				let nodeA = new TestNode(Status.Failed);
				nodeA.setRunning(1);
				stateSelector.stateToChildMap.set("a", nodeA);

				let params = createTickParams();

				test.act();
				let result = stateSelector.tick(params);
				result.catch(() => { });

				await new Promise(resolve => setTimeout(resolve));

				params.onUpdate.trigger();

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(result.status, Status.Failed);
			});

			suite.test("When a state runs, and an update is called, " +
				"then check for a state change before updating the existing state.",
				async test => {
					test.arrange();
					let count = 0;
					let stateSelector = new StateSelector(() => {
						count++;
						return count === 1 ? "a" : "b";
					});

					let nodeA = new TestNode(Status.Failed);
					nodeA.setRunning(1);
					stateSelector.stateToChildMap.set("a", nodeA);

					let nodeB = new TestNode(Status.Succeeded);
					nodeB.setRunning(1);
					stateSelector.stateToChildMap.set("b", nodeB);

					let params = createTickParams();

					test.act();
					let result = stateSelector.tick(params);
					result.catch(() => { });
					assert.equal(nodeA.tickCount, 1);
					assert.equal(result.status, Status.Running);

					await new Promise(resolve => setTimeout(resolve));

					params.onUpdate.trigger();

					await new Promise(resolve => setTimeout(resolve));

					assert.equal(nodeB.tickCount, 1);
					assert.equal(result.status, Status.Running);
					params.onUpdate.trigger();

					await new Promise(resolve => setTimeout(resolve));

					test.assert();
					assert.equal(result.status, Status.Succeeded);
				});

			suite.test("When a state changes while running, should cancel and change state.", async test => {
				test.arrange();
				let count = 0;
				let stateSelector = new StateSelector(() => {
					count++;
					return count == 1 ? "a" : "b";
				});

				let nodeA = new TestNode(Status.Failed);
				nodeA.setRunning(10);
				stateSelector.stateToChildMap.set("a", nodeA);

				let nodeB = new TestNode(Status.Succeeded);
				stateSelector.stateToChildMap.set("b", nodeB);

				let params = createTickParams();

				test.act();
				let result = stateSelector.tick(params);
				result.catch(() => { });
				assert.equal(nodeA.tickCount, 1);
				assert.equal(result.status, Status.Running);

				await new Promise(resolve => setTimeout(resolve));

				params.onUpdate.trigger();

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(nodeB.tickCount, 1);
				assert.equal(result.status, Status.Succeeded);
			});

			suite.test("When a state changes(cancelled), " +
				"and the stateSelector is cancelled right after in the same cycle, " +
				"should cancel and not break(bug fix).", async test => {
					test.arrange();
					let count = 0;
					let stateSelector = new StateSelector(() => {
						count++;
						return count == 1 ? "a" : "b";
					});

					let nodeA = new TestNode(Status.Failed);
					nodeA.setRunning(10);
					stateSelector.stateToChildMap.set("a", nodeA);

					let nodeB = new TestNode(Status.Succeeded);
					stateSelector.stateToChildMap.set("b", nodeB);

					let cancelSource = new CancelTokenSource();
					let params = createTickParams(cancelSource);

					test.act();
					let result = stateSelector.tick(params);
					result.catch(() => { });
					assert.equal(nodeA.tickCount, 1);
					assert.equal(result.status, Status.Running);

					await new Promise(resolve => setTimeout(resolve));

					params.onUpdate.trigger();
					// The bug caused an error in the cancel listener.
					// It does not fail the test! But the error is printed.
					cancelSource.cancel();

					await new Promise(resolve => setTimeout(resolve));

					test.assert();
					assert.equal(result.status, Status.Cancelled);
				});

			suite.test("When a state changes from an unknown state, should NOT throw (bug fix).", async test => {
				test.arrange();
				let count = 0;
				let stateSelector = new StateSelector(() => {
					count++;
					return count++ == 1 ? "a" : "b";
				});

				let params = createTickParams();

				test.act();
				let result = stateSelector.tick(params);
				result.catch(() => { });

				// The StateSelector is always running at tick.
				// Assert anyway because this bug and test depends on this fact.
				assert.equal(result.status, Status.Running);

				params.onUpdate.trigger();

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(result.status, Status.Failed);
			});
		});
	});
}