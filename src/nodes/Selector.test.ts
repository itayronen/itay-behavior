import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { Event } from "itay-events";
import { Status, CancelToken } from "task-run";
import { TestNode, TickParams } from "./imports";
import { Selector } from "./Selector";
import { BehaviorTree } from "./imports";

function createTickParams(): TickParams {
	return { agent: {}, tree: null!, cancelToken: CancelToken.none, onUpdate: new Event() };
}

export default function (suite: TestSuite): void {
	suite.describe(Selector.name, suite => {
		suite.describe("tick", suite => {

			suite.test("No children, should fail.", test => {
				test.arrange();
				let selector = new Selector([]);

				test.act();
				let result = selector.tick(createTickParams());
				result.catch(() => { });

				test.assert();
				assert.equal(result.status, Status.Failed);
			});

			suite.test("First child succeeds, second child fails, should succeed.", test => {
				test.arrange();
				let selector = new Selector([]);
				selector.children.push(new TestNode(Status.Succeeded));
				selector.children.push(new TestNode(Status.Failed));

				test.act();
				let result = selector.tick(createTickParams());

				test.assert();
				assert.equal(result.status, Status.Succeeded);
			});

			suite.test("First child runs then succeeds, second child fails, should succeed.", async test => {
				test.arrange();
				let selector = new Selector([]);

				let firstChild = new TestNode(Status.Succeeded);
				firstChild.setRunning(1);
				selector.children.push(firstChild);

				selector.children.push(new TestNode(Status.Failed));

				let params = createTickParams();

				test.act();
				let result = selector.tick(params);
				assert.isTrue(result.isRunning);

				params.onUpdate.trigger();

				await result.end;

				test.assert();
				assert.isTrue(result.isSucceded);
			});

			suite.test("When first child fails and second child succeeds, succeed.", test => {
				test.arrange();
				let selector = new Selector();
				selector.children.push(new TestNode(Status.Failed));
				let second = new TestNode(Status.Succeeded);
				selector.children.push(second);

				test.act();
				let result = selector.tick(createTickParams());

				test.assert();
				assert.equal(result.status, Status.Succeeded);
				assert.equal(second.tickCount, 1);
			});

			suite.test("When first runs then fails, and second runs and succeeds, succeed.", async test => {
				test.arrange();
				let selector = new Selector();

				let firstChild = new TestNode(Status.Failed);
				firstChild.setRunning(1);
				selector.children.push(firstChild);

				let secondChild = new TestNode(Status.Succeeded);
				secondChild.setRunning(1);
				selector.children.push(secondChild);

				let params = createTickParams();

				test.act();
				let result = selector.tick(params);
				assert.isTrue(result.isRunning);

				await new Promise(resolve => setTimeout(resolve));

				params.onUpdate.trigger();
				assert.isTrue(result.isRunning);

				await new Promise(resolve => setTimeout(resolve));
				params.onUpdate.trigger();

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.equal(result.status, Status.Succeeded);

				await result.end;
			});

			suite.test("When no child succeded, fail.", test => {
				test.arrange();
				let selector = new Selector([]);
				selector.children.push(new TestNode(Status.Failed));
				selector.children.push(new TestNode(Status.Failed));

				test.act();
				let result = selector.tick(createTickParams());
				result.catch(() => { });

				test.assert();
				assert.equal(result.status, Status.Failed);
			});
		});
	});
}