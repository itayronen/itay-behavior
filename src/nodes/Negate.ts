import { Task, Tasks, Status } from "task-run";
import { Node, TickParams } from "./imports";

export class Negate implements Node {
	public label: string = Negate.name;

	constructor(public child: Node) {
	}

	public tick(params: TickParams): Task<void> {
		let childResult = this.child.tick(params);

		if (childResult.isSucceded) {
			return Tasks.failed("Failed - due to negation.");
		}
		else if (childResult.isFailed) {
			return Tasks.succeded<void>(undefined);
		}
		else if (childResult.isCancelled) {
			return childResult;
		}
		else {
			return Tasks.run(async () => {
				await childResult.end;

				params.cancelToken.throwIfCancelRequested();

				if (childResult.isSucceded) {
					throw "Failed - due to negation.";
				}
				else if (childResult.isFailed) {
					childResult.catch(() => { });
				}
				else {
					console.error("Should not happen. Did you throw a new CancelError() instead of using the token?");
					await childResult;
				}
			});
		}
	}
}