import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { Event } from "itay-events";
import { Status, CancelToken, CancelTokenSource } from "task-run";
import { TestNode, TickParams } from "./imports";
import { Sequence } from "./Sequence";

function createTickParams(cancelSource?: CancelTokenSource): TickParams {
	return {
		agent: {},
		tree: null!,
		cancelToken: cancelSource ? cancelSource.token : CancelToken.none,
		onUpdate: new Event()
	};
}

export default function (suite: TestSuite): void {
	suite.describe(Sequence.name, suite => {
		suite.describe("tick", suite => {

			suite.test("When No children, then succeeds.", test => {
				test.arrange();
				let sequence = new Sequence([]);

				test.act();
				let result = sequence.tick(createTickParams());

				test.assert();
				assert.equal(result.status, Status.Succeeded);
			});

			suite.test("First child succeded, then succeeds.", test => {
				test.arrange();
				let sequence = new Sequence([new TestNode()]);
				let params = createTickParams();

				test.act();
				let result = sequence.tick(params);

				test.assert();
				assert.equal(result.status, Status.Succeeded);
			});

			suite.test("When first fails, then fails.", test => {
				test.arrange();
				let sequence = new Sequence([new TestNode(Status.Failed), new TestNode()]);
				let params = createTickParams();

				test.act();
				let result = sequence.tick(params);
				result.catch(() => { });

				test.assert();
				assert.equal(result.status, Status.Failed);
			});

			suite.test("When second child fails, then fails.", test => {
				test.arrange();
				let sequence = new Sequence([new TestNode(), new TestNode(Status.Failed)]);
				let params = createTickParams();

				test.act();
				let result = sequence.tick(params);
				result.catch(() => { });

				test.assert();
				assert.equal(result.status, Status.Failed);
			});

			suite.test("First child and second child succeded, then succeeds.", test => {
				test.arrange();
				let sequence = new Sequence([new TestNode(), new TestNode()]);
				let params = createTickParams();

				test.act();
				let result = sequence.tick(params);

				test.assert();
				assert.equal(result.status, Status.Succeeded);
			});

			suite.test("When first is running then succeded, then status is running then succeded.", async test => {
				test.arrange();
				let runningToSuccess = new TestNode();
				runningToSuccess.setRunning(1);

				let sequence = new Sequence([runningToSuccess]);
				let params = createTickParams();

				test.act();
				let result = sequence.tick(params);

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.isTrue(result.isRunning);

				params.onUpdate.trigger();
				await new Promise(resolve => setTimeout(resolve));

				assert.isTrue(result.isSucceded);
			});

			suite.test("When second is running then succeded, then status is running then succeded.", async test => {
				test.arrange();
				let first = new TestNode();

				let runningToSuccess = new TestNode();
				runningToSuccess.setRunning(1);

				let sequence = new Sequence([first, runningToSuccess]);
				let params = createTickParams();

				test.act();
				let result = sequence.tick(params);

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.isTrue(result.isRunning);

				params.onUpdate.trigger();
				await new Promise(resolve => setTimeout(resolve));

				assert.isTrue(result.isSucceded);
			});

			suite.test("When second is running then cancelled, then status is running then cancelled.", async test => {
				test.arrange();
				let first = new TestNode();

				let runningToSuccess = new TestNode();
				runningToSuccess.setRunning(1);

				let sequence = new Sequence([first, runningToSuccess]);
				let cancelSource = new CancelTokenSource();
				let params = createTickParams(cancelSource);

				test.act();
				let result = sequence.tick(params);
				result.catch(() => { });

				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.isTrue(result.isRunning);

				cancelSource.cancel();
				await new Promise(resolve => setTimeout(resolve));

				assert.isTrue(result.isCancelled);
				assert.isTrue(runningToSuccess.isCancelled);
			});
		});
	});
}