import { TickParams } from "./TickParams";
import { CancelToken } from "task-run";
import { BehaviorTree } from "./BehaviorTree";
import { Event } from "itay-events";

export class ChildTickParams implements TickParams {
	public agent: any;
	public tree: BehaviorTree;
	public onUpdate: Event;
	public cancelToken: CancelToken;

	constructor(private parent: TickParams) {
		this.agent = parent.agent;
		this.tree = parent.tree;
		this.cancelToken = parent.cancelToken;

		this.onUpdate = new Event();
		parent.onUpdate.add(this.handleUpdate);
	}

	public dispose(): void {
		this.parent.onUpdate.remove(this.handleUpdate);
	}

	private handleUpdate = () => {
		this.onUpdate.trigger();
	}
}