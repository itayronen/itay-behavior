import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { Status, Tasks, Task } from "task-run";
import { BehaviorTree } from "./BehaviorTree";
import { Node } from "./Node";
import { TickParams } from "./TickParams";
import { TestNode } from "./TestNode";

export default function (suite: TestSuite): void {
	suite.describe(BehaviorTree.name, suite => {
		suite.describe("tick", suite => {

			suite.test("When not running, tick root (fullTick).", test => {
				test.arrange();
				let tree = new BehaviorTree();
				let testNode = new TestNode();
				tree.child = testNode;

				let agent = {};

				test.act();
				tree.tick(agent);
				tree.tick(agent);

				test.assert();
				assert.equal(testNode.tickCount, 2);
			});

			suite.test("Fulltick ticks with refresh params.", test => {
				class MyNode implements Node {
					public label = "";
					public params: TickParams;
					public tick(params: TickParams): Task<void> {
						this.params = params;

						return Tasks.succeded<void>(undefined);
					}
				}

				test.arrange();
				let tree = new BehaviorTree();
				let node = new MyNode();
				tree.child = node;

				let agent = {};

				test.act();
				tree.tick(agent);
				let update1 = node.params.onUpdate;
				tree.tick(agent);
				let update2 = node.params.onUpdate;

				test.assert();
				assert.isTrue(update1 !== update2);
			});

			suite.test("When running and finished before next tick, tick root (fullTick).", async test => {
				test.arrange();
				let tree = new BehaviorTree();
				let testNode = new TestNode();
				testNode.setRunning(0);
				tree.child = testNode;

				let agent = {};

				test.act();
				let result = tree.tick(agent);
				assert.isTrue(result.isRunning);
				await new Promise(resolve => setTimeout(resolve));
				assert.isTrue(result.isSucceded);

				tree.tick(agent);

				test.assert();
				assert.equal(testNode.tickCount, 2);
			});

			suite.test("When running and fails before next tick, handle error and tick root (fullTick).", async test => {
				// I dont really know how to check if the error is handled.
				// Look at the output to see that there are no unhandled errors.

				test.arrange();
				let tree = new BehaviorTree();
				let testNode = new TestNode(Status.Failed);
				testNode.setRunning(0);
				tree.child = testNode;

				let agent = {};

				test.act();
				let result = tree.tick(agent);
				assert.isTrue(result.isRunning);
				await new Promise(resolve => setTimeout(resolve));

				test.assert();
				assert.isTrue(result.isFailed);
				assert.equal(testNode.tickCount, 1);
			});

			suite.test("When running, update.", test => {
				test.arrange();
				let tree = new BehaviorTree();
				let testNode = new TestNode();
				tree.child = testNode;

				testNode.setRunning(2);

				let agent = {};

				test.act();
				assert.isTrue(tree.tick(agent).isRunning);
				assert.isTrue(tree.tick(agent).isRunning);
				assert.isTrue(tree.tick(agent).isSucceded);

				test.assert();
				assert.equal(testNode.tickCount, 1);
			});
		});

		// suite.describe("Integration", suite => {
		// 	suite.test("StateSelector.", test => {
		// 		test.arrange();
		// 		let tree = new BehaviorTree();
		// 		let testNode = new TestNode();
		// 		tree.child = testNode;

		// 		let agent = {};

		// 		test.act();
		// 		tree.tick(agent);
		// 		tree.tick(agent);

		// 		test.assert();
		// 		assert.equal(testNode.tickCount, 2);
		// 	});
		// });
	});
}